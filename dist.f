        subroutine dist(m,n,d)
        implicit none
        real m1,m2,m3,n1,n2,n3,d
        real tmp1,tmp2,tmp3,a3x,a3y,a3z
        real AB(156,3),a1x,a1y,a1z,a2x,a2y,a2z
        integer m,n,i,j
        open(11,file="POSCAR")
        read(11,*)
        read(11,*)
        read(11,*) a1x, a1y, a1z
        read(11,*) a2x, a2y, a2z
        read(11,*) a3x, a3y, a3z
        read(11,*)
        read(11,*)
        read(11,*)
        do i=1,156
        read(11,*) AB(i,1), AB(i,2), AB(i,3)
        end do
        
        m1=AB(m,1)*a1x + AB(m,2)*a2x
        m2=AB(m,1)*a1y + AB(m,2)*a2y
        m3=AB(m,3)*a3z
        
        n1=AB(n,1)*a1x + AB(n,2)*a2x
        n2=AB(n,1)*a1y + AB(n,2)*a2y
        n3=AB(n,3)*a3z

        tmp1 = (m1-n1)*(m1-n1)
        tmp2 = (m2-n2)*(m2-n2)
        tmp3 = (m3-n3)*(m3-n3)
        d = sqrt(tmp1+tmp2+tmp3)
        write(*,*) m,n,d
        close (11)
        end subroutine 
