         subroutine hamiltonian(kx,ky,Hm,Gamma0,Gm,st,f) 
         implicit none 
         integer :: flag, i,j 
         double complex :: Hm(156,156) 
         double complex :: G0, ans1 
!         real kx,ky, a1x, a1y, a2x, a2y, junk, rcut, dist              
!         real AB(218,2),R1(2),R2(2),R3(2),R4(2), Rm(2)                 
!         real R5(2), r6(2), R7(2), R8(2), R9(2)                        
         real :: Gm(6),st,f,cut1,cut2,d 
         real :: Gamma0,kx,ky,Gamma1,Gamma4 
                                                                        
         do i=1,156 
           Hm(i,i) = (0.0, 0.0) 
         end do 
                                                                        
         ! onsite terms of graphane                                     
         do i = 57, 107 
           Hm(i,i) = f*(Gm(1)+(2*Gm(1)*st)) 
         end do 
                                                                        
         do i = 107, 156 
         Hm(i,i) = 0.4 
         end do 
                                                                        
         ! onsite term of graphene                                      
         do i = 1,56 
          do j = 57,107 
          call dist(i,j,d) 
           if (d .le. 2.7) then 
           Hm(i,i) = f*(Gm(1)+(2*Gm(1)*st)) 
           end if 
          end do 
          end do 
                                                                        
         G0 = cmplx(Gamma0,0.0) 
                                                                        
         flag = 1 
! Upper half of the hamiltonian                                         
                                                                        
!         ! mono layer components of the hamiltonian                    
         do i = 1,56 
             do j = 1,56 
         if (j .gt.i) then 
         cut1 = 1.2 
         cut2 = 1.6 
         call fnintL(i,j,flag,kx,ky,ans1,cut1,cut2) 
         write(*,*) i, j 
         Hm(i,j) = Gamma0*ans1 
         write(*,*) i, j, Hm(i,j) 
         end if 
         end do 
         end do 
                                                                        
         do i = 57,106 
             do j = 57,106 
         if (j .gt.i) then 
         cut1 = 1.2 
         cut2 = 1.6 
         call fnintL(i,j,flag,kx,ky,ans1,cut1,cut2) 
         Hm(i,j) = Gamma0*ans1 
         write(*,*) i, j, Hm(i,j) 
         end if 
         end do 
         end do 
                                                                        
! interlayer coupling between C and CH                                  
        do i = 1,56 
          do j = 57,106 
                                                                        
         cut1 = 3.6 
         cut2 = 3.9 
         call fnintL(i,j,flag,kx,ky,ans1,cut1,cut2) 
         Hm(i,j) = (Gm(1)+(2*Gm(1)*st))*ans1 
          write(*,*) i, j, Hm(i,j) 
                                                                        
         cut1 = 3.9 
         cut2 = 4.2 
         call fnintL(i,j,flag,kx,ky,ans1,cut1,cut2) 
         Hm(i,j) = (Gm(2)+(2*Gm(2)*st))*ans1 
             write(*,*) i, j, Hm(i,j) 
                                                                        
         cut1 = 4.2 
         cut2 = 4.7 
         call fnintL(i,j,flag,kx,ky,ans1,cut1,cut2) 
         Hm(i,j) = (Gm(3)+(2*Gm(3)*st))*ans1 
              write(*,*) i, j, Hm(i,j) 
                                                                        
            end do 
           end do 
                                                                        
!          interlayer coupling between c and H of CH                    
            do i = 57, 106 
              do j = 107, 156 
                                                                        
             cut1 = 1.0 
         cut2 = 1.3 
         call fnintL(i,j,flag,kx,ky,ans1,cut1,cut2) 
         Hm(i,j) = (Gm(4)+(2*Gm(4)*st))*ans1 
              write(*,*) i, j, Hm(i,j) 
             end do 
            end do 
                                                                        
!          interlayer coupling between C of gr and H of CH              
            do i = 1, 56 
              do j = 107, 131 
          cut1 = 2.5 
         cut2 = 2.9 
          call fnintL(i,j,flag,kx,ky,ans1,cut1,cut2) 
          Hm(i,j) = (Gm(5)+(2*Gm(5)*st))*ans1 
              write(*,*) i, j, Hm(i,j) 
            end do 
           end do 
                                                                        
! Lower half of the hamiltonian                                         
         flag = 2 
                                                                        
!         ! mono layer components of the hamiltonian                    
         do i = 1,56 
             do j = 1,56 
         if (j .gt.i) then 
         cut1 = 1.2 
         cut2 = 1.6 
         call fnintL(i,j,flag,kx,ky,ans1,cut1,cut2) 
         write(*,*) i, j 
         Hm(i,j) = Gamma0*ans1 
         write(*,*) i, j, Hm(i,j) 
         end if 
         end do 
         end do 
                                                                        
         do i = 57,106 
             do j = 57,106 
         if (j .gt.i) then 
         cut1 = 1.2 
         cut2 = 1.6 
         call fnintL(i,j,flag,kx,ky,ans1,cut1,cut2) 
         Hm(i,j) = Gamma0*ans1 
         write(*,*) i, j, Hm(i,j) 
         end if 
         end do 
         end do 
                                                                        
! interlayer coupling between C and CH                                  
        do i = 1,56 
          do j = 57,106 
                                                                        
         cut1 = 3.6 
         cut2 = 3.9 
         call fnintL(i,j,flag,kx,ky,ans1,cut1,cut2) 
         Hm(i,j) = (Gm(1)+(2*Gm(1)*st))*ans1 
          write(*,*) i, j, Hm(i,j) 
                                                                        
         cut1 = 3.9 
         cut2 = 4.2 
         call fnintL(i,j,flag,kx,ky,ans1,cut1,cut2) 
         Hm(i,j) = (Gm(2)+(2*Gm(2)*st))*ans1 
             write(*,*) i, j, Hm(i,j) 
                                                                        
         cut1 = 4.2 
         cut2 = 4.7 
         call fnintL(i,j,flag,kx,ky,ans1,cut1,cut2) 
         Hm(i,j) = (Gm(3)+(2*Gm(3)*st))*ans1 
              write(*,*) i, j, Hm(i,j) 
                                                                        
            end do 
           end do 
                                                                        
!          interlayer coupling between c and H of CH                    
            do i = 57, 106 
              do j = 107, 156 
                                                                        
             cut1 = 1.0 
         cut2 = 1.3 
         call fnintL(i,j,flag,kx,ky,ans1,cut1,cut2) 
         Hm(i,j) = (Gm(4)+(2*Gm(4)*st))*ans1 
              write(*,*) i, j, Hm(i,j) 
             end do 
            end do 
                                                                        
!          interlayer coupling between C of gr and H of CH              
            do i = 1, 56 
              do j = 107, 131 
          cut1 = 2.5 
         cut2 = 2.9 
          call fnintL(i,j,flag,kx,ky,ans1,cut1,cut2) 
          Hm(i,j) = (Gm(5)+(2*Gm(5)*st))*ans1 
              write(*,*) i, j, Hm(i,j) 
            end do 
           end do 
                                                                        
      END                                           
