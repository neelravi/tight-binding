        subroutine Fn(kx,ky,flag,ans)
cccccccccccccccccccccccccccccccccccc
cccccccc input: kpoints     cccccccc
cccccccc flag = 1 : f(k)    cccccccc
cccccccc flag = 2 : f*(k)   cccccccc
cccccccc inplane hopping fn cccccccc
cccccccccccccccccccccccccccccccccccc
      
        implicit none
        integer flag
        real kx, ky, fIm, fR, t1, t2, a
        character junk
        complex*16 ans
        open(11,file="tb-para")
        read(11,*) junk, a
        t1 = kx*a/(sqrt(3.0))
        t2 = ky*a/2.0
        fR = cos(t1) + 2.0*(cos(t2))*(cos(t1/2.0))
        fIm = sin(t1) - 2.0*(cos(t2))*(sin(t1/2.0))
         if (flag.eq.1) ans = cmplx(fR, fIm)
         if (flag.eq.2) ans = cmplx(fR, (-1)*fIm)
        close(11)
        end subroutine
