        program tight_binding
        implicit none 
!cccccccccccccccccccccccccccccccccccccccccccc                           
!cccccccc Tight binding program for ccccccccc                           
!cccccccc bilayer graphene          ccccccccc                           
!cccccccc Reads input from POSCAR   ccccccccc                           
!cccccccccccccccccccccccccccccccccccccccccccc                           
!cc how to run cccccccccccccccccccccccccccccc                           
!cc gfortran -c *.f ccccccccccccccccccccccccc                           
!cc gfortran *.o -o main cccccccccccccccccccc                           
!cc ./main cccccccccccccccccccccccccccccccccc                           
                                                                        
        real :: k, r, temp,G0,Gamma0, k1,k2 
        real :: Gamma1, Gamma4, ezz, c, Gm(6),st,f 
        integer  :: i,ii,j,l, dim, nkpt,t 
        character :: jnk 
        double precision, allocatable :: E(:,:) 
        double precision, allocatable :: kx(:) , ky(:) 
        double precision, allocatable :: b(:) 
        double complex, allocatable   :: Hm(:,:) 
        double complex, allocatable   :: Hm0(:,:) 
        double complex, allocatable   :: deltaH(:,:)
        double complex, allocatable   :: Sm(:,:) 
        double complex, allocatable   :: A(:,:)
                                                                        
! AB stacking                                                           
       ! write(*,*) "Enter hopping parameter C-CH" !3-1,4-1,3-2,Cp-Hs,C-
       ! value-0.14,0.38,0.381,5.8,assumed 1.5                          
! read interlayer hopping parameters                                    
       !  read(*,*) Gm(5) !Gm(1),Gm(2),Gm(3),Gm(4),Gm(5)                
         Gm(1) = 0.38 
         Gm(2) = 0.381 
         Gm(3) = 0.14 
         Gm(4) = 4.965 
         Gm(5) = 0.04 
                                                                        
! generate kpoints                                                      
!        call kpoints                                                   
        open(111,file="IBZKPT-42") 
        open(100,file="tb-para") 
! read intraplane hopping parameter                                     
         read(100,*) jnk, a 
         write(*,*)a 
         do i = 2,3 
         read(100,*) jnk, G0 
        end do 
        read(100,*) jnk, Gamma0 
        close(100) 
                   ! matrix dimension                                   
        dim = 156 
                                                                        
        read(111,*) nkpt 
        allocate(Hm(dim,dim)) 
        allocate(Hm0(dim,dim)) 
        allocate(deltaH(dim,dim)) 
        allocate(A(dim,dim)) 
        allocate(Sm(dim,dim)) 
        allocate(E(nkpt,dim)) 
        allocate(kx(nkpt)) 
        allocate(ky(nkpt)) 
        allocate(b(dim)) 
                                                                        
! read KPOINTS, form hamiltonian and solve it for each point for differe
!         do l = 1,40,10                                                
!             st = 10/100                                               
!              f=0.d0                                                   
                                                                        
!          write(*,*) "Enter the value of strain and parameter f"       
!         read(*,*) st,f                                                
                                                                        
         st = 0 
         f = 0 
         do i = 1, nkpt 
         read(111,*) kx(i), ky(i) 
         Hm0 = (0.00,0.00) 
         Sm = (0.00,0.00) 
         call hamiltonian(kx(i),ky(i),Hm0,Gamma0,Gm,st,f) 
                                                                        
! lapack routine rewrites the input hamiltonian, so copied to A         
        A = Hm0 
!         do t=1,4                                                      
!         write(*,*) (A(t,j), j=1,4)                                    
!         enddo                                                         
                                                                        
! solve hamiltonian                                                     
        call Eigenvalue(A,b,dim) 
! save eigenvalues in E for each kpoint                                 
        do j = 1, dim 
            E(i,j) = b(j) 
        end do 
        end do 
        close(111) 
                                                                        
! write band structure file                                             
        open(23,file="band-str.dat") 
        k = 0.00 
        write(23,*) k, (E(1,j), j=1,dim) 
        do i = 2, nkpt 
        r=sqrt((kx(i)-kx(i-1))**2+(ky(i)-ky(i-1))**2) 
        k = k + r 
        write(23,*) k, (E(i,j), j=1,dim) 
        end do 
         close(100) 
         close(23) 
                                                                        
      END                                           
