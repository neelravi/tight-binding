        subroutine fnintL(m,n,flag,kx,ky,ans,cut1,cut2) 
!cccccccccccccccccccccccccccccccccccccccccccc                           
!ccccccc calculates hopping function cccccccc                           
!ccccccc for interlayer coupling     cccccccc                           
!ccccccc flag = 1 : f(k)             cccccccc                           
!ccccccc flag = 2 : f*(k)            cccccccc                           
!cccccccccccccccccccccccccccccccccccccccccccc                           
        implicit none 
        integer a, flag, m,n,i 
        complex*16 ans 
        real kx,ky,a1x,a1y,a1z,a2x,a2y,a2z,a3x,a3y,a3z 
        real junk, cut1, cut2, dist 
        real AB(156,3),R1(3),R2(2),R3(2),R4(2), Rm(3) 
        real R5(2), r6(2), R7(2), R8(2), R9(2) 
        real AA1x,AA1y,AA2x,AA2y,BB1x,BB1y,BB2x,BB2y 
        real d1, rr1, rr2, rr3, rr4, s1, s2, s3, s4,minrr,tmp 
        real rr5, rr6, rr7, rr8, rr9, s5, s6, s7, s8, s9 
        real th1,th2,th3,th4,fIm, fR,t1,t2,th5,th6,th7,th8,th9 
!        rcut=0.2                                                       
! read input coordinates and lattice vector from POSCAR                 
        open(11,file="POSCAR") 
        read(11,*) 
        read(11,*) 
        read(11,*) a1x, a1y, a1z 
        read(11,*) a2x, a2y, a2z 
        read(11,*) a3x, a3y, a3z 
        read(11,*) 
        read(11,*) 
        read(11,*) 
        do i=1,156 
        read(11,*) AB(i,1), AB(i,2), AB(i,3) 
        end do 
! the hamiltonian is as follows                                         
!       ! 1  2  3  4                                                    
!       ! 1                                                             
!       ! 2 matrix elements (m,n) for interactions                      
!       ! 3                                                             
!       ! 4                                                             
                                                                        
        call dista(AB(m,1),AB(m,2),AB(m,3),AB(n,1),AB(n,2),AB(n,3),d1) 
        if (d1.le.0.0001) then 
        ans = cmplx(1.00,0.00) 
! if atom of one layer is on top of the other layer,                    
! matrix element has only the hopping parameter gamma1                  
! One nn in the othe layer                                              
        goto 222 
        end if 
!       ! AB(m,1) AB(m,2) in cartesian coordinate                       
! check for nn of mth aotom on the opposite layer                       
        Rm(1)=AB(m,1)*a1x + AB(m,2)*a2x 
        Rm(2)=AB(m,1)*a1y + AB(m,2)*a2y 
        Rm(3)=AB(m,3)*a3z 
                                                                        
!       ! coordinates of the nearest neighbours cartesian               
                                                                        
        R1(1)=AB(n,1)*a1x + AB(n,2)*a2x 
        R1(2)=AB(n,1)*a1y + AB(n,2)*a2y 
        R1(3)=AB(n,3)*a3z 
        R2(1)=(AB(n,1)-1)*a1x + AB(n,2)*a2x 
        R2(2)=(AB(n,1)-1)*a1y + AB(n,2)*a2y 
        R3(1)=AB(n,1)*a1x + (AB(n,2)-1)*a2x 
        R3(2)=AB(n,1)*a1y + (AB(n,2)-1)*a2y 
        R4(1)=(AB(n,1)-1)*a1x + (AB(n,2)-1)*a2x 
        R4(2)=(AB(n,1)-1)*a1y + (AB(n,2)-1)*a2y 
        R5(1)=(AB(n,1)+1)*a1x + AB(n,2)*a2x 
        R5(2)=(AB(n,1)+1)*a1y + AB(n,2)*a2y 
        R6(1)=AB(n,1)*a1x + (AB(n,2)+1)*a2x 
        R6(2)=AB(n,1)*a1y + (AB(n,2)+1)*a2y 
        R7(1)=(AB(n,1)+1)*a1x + (AB(n,2)+1)*a2x 
        R7(2)=(AB(n,1)+1)*a1y + (AB(n,2)+1)*a2y 
        R8(1)=(AB(n,1)+1)*a1x + (AB(n,2)-1)*a2x 
        R8(2)=(AB(n,1)+1)*a1y + (AB(n,2)-1)*a2y 
        R9(1)=(AB(n,1)-1)*a1x + (AB(n,2)+1)*a2x 
        R9(2)=(AB(n,1)-1)*a1y + (AB(n,2)+1)*a2y 
                                                                        
! calculate hopping function (real and imaginary part)                  
!       ! fIm, fR                                                       
        th1=(kx*R1(1)) + (ky*R1(2)) 
        th2=(kx*R2(1)) + (ky*R2(2)) 
        th3=(kx*R3(1)) + (ky*R3(2)) 
        th4=(kx*R4(1)) + (ky*R4(2)) 
        th5=(kx*R5(1)) + (ky*R5(2)) 
        th6=(kx*R6(1)) + (ky*R6(2)) 
        th7=(kx*R7(1)) + (ky*R7(2)) 
        th8=(kx*R8(1)) + (ky*R8(2)) 
        th9=(kx*R9(1)) + (ky*R9(2)) 
                                                                        
        s1=0.0 
        s2=0.0 
        s3=0.0 
        s4=0.0 
        s5=0.0 
        s6=0.0 
        s7=0.0 
        s8=0.0 
        s9=0.0 
                                                                        
!       ! distance of these points from the given site                  
                                                                        
        call dista(R1(1),R1(2),R1(3),Rm(1),Rm(2),Rm(3),rr1) 
        if (rr1 .gt. cut1 .and. rr1 .lt. cut2) s1 = 1 
                                                                        
        call dista(R2(1),R2(2),R1(3),Rm(1),Rm(2),Rm(3),rr2) 
        if (rr2 .gt. cut1 .and. rr2 .lt. cut2) s2 = 1 
                                                                        
        call dista(R3(1),R3(2),R1(3),Rm(1),Rm(2),Rm(3),rr3) 
        if (rr3 .gt. cut1 .and. rr3 .lt. cut2) s3 = 1 
                                                                        
        call dista(R4(1),R4(2),R1(3),Rm(1),Rm(2),Rm(3),rr4) 
        if (rr4 .gt. cut1 .and. rr4 .lt. cut2) s4 = 1 
                                                                        
        call dista(R5(1),R5(2),R1(3),Rm(1),Rm(2),Rm(3),rr5) 
        if (rr5 .gt. cut1 .and. rr5 .lt. cut2) s5 = 1 
                                                                        
        call dista(R6(1),R6(2),R1(3),Rm(1),Rm(2),Rm(3),rr6) 
        if (rr6 .gt. cut1 .and. rr6 .lt. cut2) s6 = 1 
                                                                        
        call dista(R7(1),R7(2),R1(3),Rm(1),Rm(2),Rm(3),rr7) 
        if (rr7 .gt. cut1 .and. rr7 .lt. cut2) s7 = 1 
                                                                        
        call dista(R8(1),R8(2),R1(3),Rm(1),Rm(2),Rm(3),rr8) 
        if (rr8 .gt. cut1 .and. rr8 .lt. cut2) s8 = 1 
                                                                        
        call dista(R9(1),R9(2),R1(3),Rm(1),Rm(2),Rm(3),rr9) 
        if (rr9 .gt. cut1 .and. rr9 .lt. cut2) s9 = 1 
                                                                        
                                                                        
! f = SUM{exp(ik.Rj)}, j=1.....nn                                       
        t1=(s1*cos(th1))+(s2*cos(th2))+(s5*cos(th5))                    &
     &      +(s6*cos(th6))+(s7*cos(th7))                                
        t2=(s3*cos(th3))+(s4*cos(th4))+(s8*cos(th8))+(s9*cos(th9)) 
        fR=t1+t2 
!        write(*,*) fR                                                  
        t1=(s1*sin(th1))+(s2*sin(th2))+(s5*sin(th5))                    &
     &      +(s6*sin(th6))+(s7*sin(th7))                                
        t2=(s3*sin(th3))+(s4*sin(th4))+(s8*sin(th8))+(s9*sin(th9)) 
        fIm=t1+t2 
!        write(*,*) fIm                                                 
        if (flag.eq.1) ans = cmplx(fR, fIm) 
        if (flag.eq.2) ans = cmplx(fR, (-1)*fIm) 
                                                                        
  222   close(11) 
                                                                        
        end subroutine 
                                                                        
                                                                        
                                                                        
        subroutine dista(m1,m2,m3,n1,n2,n3,d) 
        real m1,m2,m3,n1,n2,n3,d,tmp1,tmp2,tmp3 
        tmp1 = (m1-n1)*(m1-n1) 
        tmp2 = (m2-n2)*(m2-n2) 
        tmp3 = (m3-n3)*(m3-n3) 
        d = sqrt(tmp1+tmp2+tmp3) 
      END                                           
