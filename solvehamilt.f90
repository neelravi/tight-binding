        subroutine Eigenvalue(A,b,N) 
        Implicit none 
!cccccccccccccccccccccccccccccccccccccccccccccccccc                     
!ccccccc Solves hamiltonian of dimension N cccccccc                     
!cccccccccccccccccccccccccccccccccccccccccccccccccc                     
        integer :: N 
        double complex :: A(N,N) 
        double complex , allocatable :: WORK(:) 
        double precision :: b(N) 
        double precision, allocatable :: RWORK(:) 
        character*1 JOBZ, UPLO 
        integer  :: i,j, INFO, LDA, LWORK, LR 
        double complex :: temp 
!        !           N = 2 ! order of matrix                            
!         do i=1,4                                                      
!         write(*,*) (A(i,j), j=1,4)                                    
!         enddo                                                         
        LDA = N 
                         ! -1 for optimal value                         
        LWORK =  2*N-1 
        LR =  3*N-2 
        JOBZ = 'N' 
        UPLO = 'U' 
        temp=(3.1, -1.8) 
        allocate(WORK(LWORK)) 
        allocate(RWORK(LR)) 
                                                                        
        call ZHEEV( JOBZ, UPLO, N, A, LDA, b, WORK, LWORK, RWORK, INFO ) 
!        !write(*,*) "INFO", INFO                                       
      END                                           
